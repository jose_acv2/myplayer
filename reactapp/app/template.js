<!DOCTYPE html>
<html>
  <head>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href="//cdn.muicss.com/mui-0.1.2/css/mui.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <title>{%=o.htmlWebpackPlugin.options.title%}</title>

  </head>
  <body>
  </body>
</html>